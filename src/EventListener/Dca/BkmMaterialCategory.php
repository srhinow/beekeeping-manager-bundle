<?php
/**
 * Created by c4.pringitzhonig.de.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 15.09.19
 */

namespace Srhinow\BeekeepingManagerBundle\EventListener\Dca;


use Contao\Backend;
use Contao\DataContainer;

class BkmMaterialCategory extends Backend
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Autogenerate an material-category alias if it has not been set yet
     * @param mixed
     * @param object
     * @return string
     */
    public function generateAlias($varValue, DataContainer $dc)
    {
        $autoAlias = false;

        // Generate alias if there is none
        if (!strlen($varValue)) {
            $autoAlias = true;
            $varValue = standardize($dc->activeRecord->title);
        }

        $objAlias = $this->Database->prepare("SELECT id FROM tl_bkm_material_category WHERE id=? OR alias=?")
            ->execute($dc->id, $varValue);

        // Check whether the page alias exists
        if ($objAlias->numRows > 1) {
            if (!$autoAlias) {
                throw new Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
            }

            $varValue .= '-' . $dc->id;
        }

        return $varValue;
    }
}