<?php
/**
 * Created by c4.pringitzhonig.de.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 09.09.19
 */

namespace Srhinow\BeekeepingManagerBundle\EventListener\Dca;


use Contao\Backend;
use Contao\Controller;
use Contao\Database;
use Contao\Image;
use Srhinow\BeekeepingManagerBundle\Model\BkmBeehiveModel;
use Srhinow\BeekeepingManagerBundle\Model\BkmColoniesModel;

class BkmLocation extends Backend
{
    /**
     * Import the back end user object
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param \DataContainer $dc
     */
    public function setGeoLocation(\DataContainer $dc)
    {
        if(strlen(($dc->activeRecord->geo_lat) < 1 || strlen($dc->activeRecord->geo_lat) < 1) && strlen($dc->activeRecord->address) > 0){
            $geoApi = new orgLocationIQ\Client(LOCATION_API_KEY);
            $geoApi->setLanguage('de');
            $result = $geoApi->geocode($dc->activeRecord->address);

            if(strlen($result[0]->lat) > 0 && strlen($result[0]->lon) > 0) {
                $set = [
                    'geo_lat' =>  $result[0]->lat,
                    'geo_lon' =>  $result[0]->lon
                ];
                Database::getInstance()->prepare('UPDATE tl_bkm_location %s WHERE id=?')->set($set)->execute($dc->id);

                $this->reload();
            }

        }
    }

    public function getHivemapButton($row, $href, $label, $title, $icon, $attributes) {
        return '<a href="'.$this->addToUrl($href.'&amp;locationId='.$row['id']).'" title="'.specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label).'</a> ';
    }

    public function labelGroupCallback($arrRow, $strLabel, $dc, $args) {

        //Anzahl der dort steheneden Bienen stück zaehlen
        $args[1] = BkmColoniesModel::countBy(['pid=?','death !=?'], [$arrRow['id'],1]);


        return $args;
    }
}
