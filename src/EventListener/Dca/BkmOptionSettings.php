<?php
/**
 * Created by c4.pringitzhonig.de.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 17.09.19
 */

namespace Srhinow\BeekeepingManagerBundle\EventListener\Dca;


use Contao\Backend;
use Contao\Controller;
use Contao\DataContainer;
use Contao\Input;
use Srhinow\BeekeepingManagerBundle\Model\BkmMaterialCategoryModel;
use Srhinow\BeekeepingManagerBundle\Model\BkmMaterialModel;
use Srhinow\BeekeepingManagerBundle\Model\BkmOptionSettingsModel;

class BkmOptionSettings extends Backend
{
    protected $objOptionSettings;
    /**
     * Import the back end user object
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getOptionSettings($id=0){
        if($id == 0) {
            $this->objOptionSettings = BkmOptionSettingsModel::findBy('fallback',1);
        } else {
            $this->objOptionSettings = BkmOptionSettingsModel::findByPk($id);
        }

        if(null === $this->objOptionSettings){
            new \Exception('Es wurden keine Options-Eisntellungen als Fallback gekennzeichnet');
            exit();
        }
    }
    public function setDefaultValues() {
        if(Input::get('act') !== 'edit' && (int) Input::get('id') < 1) return;
        $this->getOptionSettings((int) Input::get('id'));

        if($this->isEmptyOptionsField($this->objOptionSettings->status_options)) $this->setDefaultStatusOptions();
        if($this->isEmptyOptionsField($this->objOptionSettings->frametype_options)) $this->getFrametypesOptions();
        if($this->isEmptyOptionsField($this->objOptionSettings->meditype_options)) $this->setDefaultMeditypeOptions();
        if($this->isEmptyOptionsField($this->objOptionSettings->volumes_options)) $this->getVolumeOptions();
        if($this->isEmptyOptionsField($this->objOptionSettings->feed_options)) $this->setDefaultFeedOptions();
        if($this->isEmptyOptionsField($this->objOptionSettings->bee_race_options)) $this->setDefaultBeeRaceOptions();
        if($this->isEmptyOptionsField($this->objOptionSettings->breed_quantity_options)) $this->setDefaultBreedQuantityOptions();
        if($this->isEmptyOptionsField($this->objOptionSettings->gentleness_options)) $this->setDefaultGentlenessOptions();
        if($this->isEmptyOptionsField($this->objOptionSettings->feed_quantity_options)) $this->setDefaultFeedQuantityOptions();
        if($this->isEmptyOptionsField($this->objOptionSettings->population_size_options)) $this->setDefaultPopulationSizeOptions();

        $this->objOptionSettings->save();
    }
    public function createDefaultValues($table, $id) {

        $this->getOptionSettings($id);
        $this->setDefaultStatusOptions();
        $this->setDefaultFrametypeOptions();
        $this->setDefaultMeditypeOptions();
        $this->setDefaultVolumesOptions();
        $this->setDefaultFeedOptions();
        $this->setDefaultBeeRaceOptions();
        $this->setDefaultBreedQuantityOptions();
        $this->setDefaultGentlenessOptions();
        $this->setDefaultFeedQuantityOptions();
        $this->setDefaultPopulationSizeOptions();
        $this->objOptionSettings->save();
    }

    protected function setDefaultStatusOptions() {

        $options = $GLOBALS['TL_LANG']['tl_bkm_option_settings']['default_status_options'];
        if(!is_array($options) || count($options) < 1) return;

        $multi = [];
        foreach($options as $k => $v) {
            $multi[] = [
              'key' => $k,
              'text' => $v
            ];
        }
        $this->objOptionSettings->status_options = $multi;
    }

    protected function setDefaultFrametypeOptions() {

        $options = $GLOBALS['TL_LANG']['tl_bkm_option_settings']['default_frametype_options'];
        if(!is_array($options) || count($options) < 1) return;

        $multi = [];
        foreach($options as $k => $v) {
            $multi[] = [
              'key' => $k,
              'text' => $v
            ];
        }
        $this->objOptionSettings->frametype_options = $multi;
    }

    protected function setDefaultMeditypeOptions() {

        $options = $GLOBALS['TL_LANG']['tl_bkm_option_settings']['default_meditype_options'];
        if(!is_array($options) || count($options) < 1) return;

        $multi = [];
        foreach($options as $k => $v) {
            $multi[] = [
              'key' => $k,
              'text' => $v
            ];
        }
        $this->objOptionSettings->meditype_options = $multi;
    }

    protected function setDefaultVolumesOptions() {

        $options = $GLOBALS['TL_LANG']['tl_bkm_option_settings']['default_volumes_options'];
        if(!is_array($options) || count($options) < 1) return;

        $multi = [];
        foreach($options as $k => $v) {
            $multi[] = [
              'key' => $k,
              'text' => $v
            ];
        }
        $this->objOptionSettings->volumes_options = $multi;
    }

    protected function setDefaultFeedOptions() {

        $options = $GLOBALS['TL_LANG']['tl_bkm_option_settings']['default_feed_options'];
        if(!is_array($options) || count($options) < 1) return;

        $multi = [];
        foreach($options as $k => $v) {
            $multi[] = [
              'key' => $k,
              'text' => $v
            ];
        }
        $this->objOptionSettings->feed_options = $multi;
    }

    protected function setDefaultBeeRaceOptions() {

        $options = $GLOBALS['TL_LANG']['tl_bkm_option_settings']['default_bee_race_options'];
        if(!is_array($options) || count($options) < 1) return;

        $multi = [];
        foreach($options as $k => $v) {
            $multi[] = [
              'key' => $k,
              'text' => $v
            ];
        }
        $this->objOptionSettings->bee_race_options = $multi;
    }

    protected function setDefaultBreedQuantityOptions() {

        $options = $GLOBALS['TL_LANG']['tl_bkm_option_settings']['default_breed_quantity_options'];
        if(!is_array($options) || count($options) < 1) return;

        $multi = [];
        foreach($options as $k => $v) {
            $multi[] = [
                'key' => $k,
                'text' => $v
            ];
        }
        $this->objOptionSettings->breed_quantity_options = $multi;
    }

    protected function setDefaultFeedQuantityOptions() {

        $options = $GLOBALS['TL_LANG']['tl_bkm_option_settings']['default_feed_quantity_options'];
        if(!is_array($options) || count($options) < 1) return;

        $multi = [];
        foreach($options as $k => $v) {
            $multi[] = [
                'key' => $k,
                'text' => $v
            ];
        }
        $this->objOptionSettings->feed_quantity_options = $multi;
    }

    protected function setDefaultPopulationSizeOptions() {

        $options = $GLOBALS['TL_LANG']['tl_bkm_option_settings']['default_population_size_options'];
        if(!is_array($options) || count($options) < 1) return;

        $multi = [];
        foreach($options as $k => $v) {
            $multi[] = [
                'key' => $k,
                'text' => $v
            ];
        }
        $this->objOptionSettings->population_size_options = $multi;
    }

    protected function setDefaultGentlenessOptions() {

        $options = $GLOBALS['TL_LANG']['tl_bkm_option_settings']['default_gentleness_options'];
        if(!is_array($options) || count($options) < 1) return;

        $multi = [];
        foreach($options as $k => $v) {
            $multi[] = [
                'key' => $k,
                'text' => $v
            ];
        }
        $this->objOptionSettings->gentleness_options = $multi;
    }

    public function getMaterialOptionsForSettings() {

        $arrReturn = [];
        $objCategories = $this->getMaterialCategoryOptions();
        if(!is_array($objCategories) || count($objCategories)<1) return $arrReturn;

        foreach($objCategories as $catk => $catv) {
            $objMaterial = BkmMaterialModel::findBy('category',$catk);
            if(null === $objMaterial) continue;

            while($objMaterial->next()) {
                $arrReturn[$catv][$objMaterial->id] =  $objMaterial->title;
//            $arrReturn[$objMaterial->id] = $objMaterial->title;
            }
        }

        return $arrReturn;
    }

    public function getMaterialCategoryOptions() {
        $arrReturn = [];
        $objMaterialCategory = BkmMaterialCategoryModel::findAll();
        if(null === $objMaterialCategory) return $arrReturn;

        while($objMaterialCategory->next()) {
            $arrReturn[$objMaterialCategory->id] = $objMaterialCategory->title;
        }

        return $arrReturn;
    }

    /**
     * @return array
     */
    public function getStatusOptions(){
        $this->getOptionSettings();
        $arrReturn = [];
        $options = unserialize($this->objOptionSettings->status_options);
        if(!is_array($options) || count($options) < 1) return $arrReturn;

        foreach($options as $k => $data){
            $arrReturn[$data['key']] = $data['text'];
        }
        return $arrReturn;
    }

    /**
     * @return array
     */
    public function getFrametypesOptions(){
        $this->getOptionSettings();
        $arrReturn = [];
        $options = unserialize($this->objOptionSettings->frametype_options);
        if(!is_array($options) || count($options) < 1) return $arrReturn;

        foreach($options as $k => $data){
            $arrReturn[$data['key']] = $data['text'];
        }
        return $arrReturn;
    }

   /**
     * @return array
     */
    public function getBeeRaceOptions(){
        $this->getOptionSettings();
        $arrReturn = [];
        $options = unserialize($this->objOptionSettings->bee_race_options);
        if(!is_array($options) || count($options) < 1) return $arrReturn;

        foreach($options as $k => $data){
            $arrReturn[$data['key']] = $data['text'];
        }
        return $arrReturn;
    }

    /**
     * @return array
     */
    public function getMeditypeOptions(){
        $this->getOptionSettings();
        $arrReturn = [];
        $options = unserialize($this->objOptionSettings->meditype_options);
        if(!is_array($options) || count($options) < 1) return $arrReturn;

        foreach($options as $k => $data){
            $arrReturn[$data['key']] = $data['text'];
        }
        return $arrReturn;
    }

    /**
     * @return array
     */
    public function getVolumeOptions(){
        $this->getOptionSettings();
        $arrReturn = [];
        $options = unserialize($this->objOptionSettings->volumes_options);
        if(!is_array($options) || count($options) < 1) return $arrReturn;

        foreach($options as $k => $data){
            $arrReturn[$data['key']] = $data['text'];
        }
        return $arrReturn;
    }

    /**
     * @return array
     */
    public function getFeedtypeOptions(){
        $this->getOptionSettings();
        $arrReturn = [];
        $options = unserialize($this->objOptionSettings->feed_options);
        if(!is_array($options) || count($options) < 1) return $arrReturn;

        foreach($options as $k => $data){
            $arrReturn[$data['key']] = $data['text'];
        }
        return $arrReturn;
    }

    /**
     * @return array
     */
    public function getMaterialOptions() {
        $this->getOptionSettings();
        $arrReturn = [];
        $options = unserialize($this->objOptionSettings->material_options);
        if(!is_array($options) || count($options) < 1) return $arrReturn;

        foreach($options as $k => $data){
            $arrReturn[$data['key']] = $data['text'];
        }
        return $arrReturn;
    }

    /**
     * @return array
     */
    public function getBreedQuantityOptions() {
        $this->getOptionSettings();
        $arrReturn = [];
        $options = unserialize($this->objOptionSettings->breed_quantity_options);
        if(!is_array($options) || count($options) < 1) return $arrReturn;

        foreach($options as $k => $data){
            $arrReturn[$data['key']] = $data['text'];
        }
        return $arrReturn;
    }

    /**
     * @return array
     */
    public function getFeedQuantityOptions() {
        $this->getOptionSettings();
        $arrReturn = [];
        $options = unserialize($this->objOptionSettings->gentleness_options);
        if(!is_array($options) || count($options) < 1) return $arrReturn;

        foreach($options as $k => $data){
            $arrReturn[$data['key']] = $data['text'];
        }
        return $arrReturn;
    }

    /**
     * @return array
     */
    public function getPopulationSizeOptions() {
        $this->getOptionSettings();
        $arrReturn = [];
        $options = unserialize($this->objOptionSettings->population_size_options);
        if(!is_array($options) || count($options) < 1) return $arrReturn;

        foreach($options as $k => $data){
            $arrReturn[$data['key']] = $data['text'];
        }
        return $arrReturn;
    }

    /**
     * @return array
     */
    public function getGentlenessOptions() {
        $this->getOptionSettings();
        $arrReturn = [];
        $options = unserialize($this->objOptionSettings->feed_quantity_options);
        if(!is_array($options) || count($options) < 1) return $arrReturn;

        foreach($options as $k => $data){
            $arrReturn[$data['key']] = $data['text'];
        }
        return $arrReturn;
    }

    /**
     * Prüft ein optionsfeld auf die verschiedenen Moeglichkeiten ob dieses leer ist
     * @param $field
     * @return bool
     */
    protected function isEmptyOptionsField($field) {
        //wenn es nul ist
        if(null === $field) return true;

        //wenn es bereits in der Db serialisiert wurde
        if(!is_array($field)) $field = unserialize($field);

        //wenn es ein Array ist
        if(is_array($field)) {

            //wenn es mehrere Reihen gibt gehe ich davon aus das diese auch befüllt sind
            if(count($field) > 1) return false;

            //wenn der Wert 'text' in der ersten Zeile keinen Inhalt hat
            return (strlen($field[0]['text']) > 0) ? false : true;
        }
        // bei allen anderen Möglichkeiten es so setzen das es neu befüllt wird
        return true;
    }
}