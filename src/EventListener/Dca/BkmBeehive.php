<?php
/**
 * Created by c4.pringitzhonig.de.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 08.09.19
 */

namespace Srhinow\BeekeepingManagerBundle\EventListener\Dca;


use Contao\Backend;
use Contao\DataContainer;
use Contao\Image;
use Srhinow\BkmFrameDimensionsModel;

class BkmBeehive extends Backend
{
    /**
     * Import the back end user object
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Return the link picker wizard
     * @param DataContainer $dc
     * @return string
     */
    public function pagePicker(DataContainer $dc)
    {
        $strField = 'ctrl_' . $dc->field . (($this->Input->get('act') == 'editAll') ? '_' . $dc->id : '');
        return ' ' . Image::getHtml('pickpage.gif', $GLOBALS['TL_LANG']['MSC']['pagepicker'], 'style="vertical-align:top; cursor:pointer;" onclick="Backend.pickPage(\'' . $strField . '\')"');
    }

    /**
     * get Frame-Dimensions for option-selection
     * @param DataContainer $dc
     * @return array
     */
    public function getFrameDimensionsOptions(DataContainer $dc)
    {
        $varValue= [];
        $objDb = BkmFrameDimensionsModel::findAll();

        while($objDb != null)
        {
            $varValue[$objDb->id] = $objDb->name;
        }

        return $varValue;
    }
}