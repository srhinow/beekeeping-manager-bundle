<?php
/**
 * Created by c4.pringitzhonig.de.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 09.09.19
 */

namespace Srhinow\BeekeepingManagerBundle\EventListener\Dca;


use Contao\Backend;
use Contao\Controller;
use Contao\Database;
use Contao\DataContainer;
use Contao\Date;
use Contao\Input;
use Contao\MemberModel;
use Contao\Message;
use Contao\System;
use Netzmacht\ContaoWorkflowBundle\Model\Step\StepModel;
use Netzmacht\ContaoWorkflowBundle\Model\Workflow\WorkflowModel;
use Srhinow\BeekeepingManagerBundle\Model\BkmBeehiveModel;
use Srhinow\BeekeepingManagerBundle\Model\BkmBreedQualityModel;
use Srhinow\BeekeepingManagerBundle\Model\BkmColoniesModel;
use Srhinow\BeekeepingManagerBundle\Model\BkmFeedQualityModel;
use Srhinow\BeekeepingManagerBundle\Model\BkmGentlenessModel;
use Srhinow\BeekeepingManagerBundle\Model\BkmHivemapModel;
use Srhinow\BeekeepingManagerBundle\Model\BkmLocationModel;
use Srhinow\BeekeepingManagerBundle\Model\BkmPopulationSizeModel;

class BkmHivemap extends Backend
{
    /**
     * Import the back end user object
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function labelGroupCallback($arrRow, $strLabel, $dc, $args) {

        if(Input::get('only') !== 'colony'){

            //Stock-Nummer und nicht die Id holen
            $objColony = BkmColoniesModel::findByPk($arrRow['pid']);
//                    dump($objColony); die();
            $objBeehive = BkmBeehiveModel::findByPk($objColony->hive_number);

            //Hivemap by Colony
            $colonyHref = Controller::addToUrl('do=bkm_location&table=tl_bkm_hivemap&only=colony&id='.$arrRow['pid'].'&rt='.REQUEST_TOKEN,true, []);
            $args[3] = '<a href="'.$colonyHref.'" title="'.$GLOBALS['TL_LANG']['tl_bkm_hivemap']['to_colony_entries_linktitle'].'">'.$objBeehive->nr.'</a>';


            //hivemap by Location
            $locationHref = Controller::addToUrl('do=bkm_hivemap&only=location&locationId='.$arrRow['location'].'&rt='.REQUEST_TOKEN,true, []);
            $args[4] = '<a href="'.$locationHref.'" title="'.$GLOBALS['TL_LANG']['tl_bkm_hivemap']['location_filter_linktitle'].'">'.$args[4].'</a>';
        }

        return $args;
    }
    
    
    public function listItems($arrRow) {
        return '<div class="tl_content_left">
                    <div style="float:left;color:#b3b3b3;padding-right:10px">[' . Date::parse($GLOBALS['TL_CONFIG']['dateFormat'], $arrRow['date']) . ']</div> 
                    <div style="float:left;">' . nl2br($arrRow['description']) . '</div>
                    <div style="clear:both;"><br></div>
                </div>';
    }

    /**
     * get custom view from colonies-options
     * @param $headline
     * @return string
     */
    public function custHeadline($headline)
    {
        if($headline != '' && Input::get('id'))
        {
            $resObj = BkmColoniesModel::findByIdOrAlias(Input::get('id'));
            if($resObj->numRows > 0)  $headline = sprintf($headline, $resObj->hive_number, $resObj->hive_notice);
        }
        return $headline;
    }

    /**
     * Die Ansicht der Stockkarte je nach Filter all, only=location, only=colony anpassen
     * @param DataContainer $dc
     * @return bool|void
     */
    public function setPallete(DataContainer $dc){

        $table = 'tl_bkm_hivemap';
        if($dc->table != $table) return;

        //nach Standort filtern
        if(Input::get('only') == 'location' && (int) Input::get('locationId') > 0) {
            $objLocation = BkmLocationModel::findByPk((int) Input::get('locationId'));
            if(null !== $objLocation){
                $GLOBALS['TL_DCA'][$table]['list']['sorting']['filter'][] = ['location = '.Input::get('locationId')];
                Message::addInfo(sprintf($GLOBALS['TL_LANG']['tl_bkm_hivemap']['messages']['filtered_by_location'], $objLocation->name));

            }
        }

        if(Input::get('only') == 'colony') {
            //Ansicht der Liste anders darstellen als in Tabellenansicht
            unset($GLOBALS['TL_DCA'][$table]['list']['label']['fields'][3]);
            unset($GLOBALS['TL_DCA'][$table]['list']['label']['fields'][4]);
            $GLOBALS['TL_DCA'][$table]['list']['sorting']['mode'] = 4;
            $GLOBALS['TL_DCA'][$table]['list']['label']['showColumns'] = false;
        } else {
            foreach($GLOBALS['TL_DCA'][$table]['palettes'] as $name => $palette) {
                //Zuordnung zu einem Stock möglich machen wenn es nicht die Colony-Ansicht ist
                if(in_array($name,['__selector__'])) continue;
                $GLOBALS['TL_DCA'][$table]['palettes'][$name] = str_replace(',date;',',pid,date;',$palette);

                //den PDF-Export-Button nicht anzeigen wenn die Stockeintraege zu mehreren Bienenvölkern gehören
                unset($GLOBALS['TL_DCA'][$table]['list']['global_operations']['pdfExport']);

                //den CSV-Export-Button nicht anzeigen wenn die Stockeintraege zu mehreren Bienenvölkern gehören
                unset($GLOBALS['TL_DCA'][$table]['list']['global_operations']['csvExport']);
            }
        }
    }
    
    /**
     * get options for item units
     * @param object
     * @return array
     */
    public function getFilteredColoniesOptions(DataContainer $dc)
    {
        $varValue= array();
        $options['order'] = 'hive_number ASC';
        if(Input::get('only') === 'location' && (int) Input::get('locationId') > 0) {
            //aktuelle Colonie zu Standortbestimmung
            $objColonies = BkmColoniesModel::findBy('pid', (int) Input::get('locationId'),$options);
        } else {
            $objColonies = BkmColoniesModel::findAll($options);
        }
        if(null === $objColonies) return $varValue;

        while($objColonies->next())
        {
            $objBeehive = BkmBeehiveModel::findByPk($objColonies->hive_number);
            $varValue[$objColonies->id] = $objBeehive->nr;
        }
        return $varValue;
    }
    /**
     * get options for item units
     * @param object
     * @return array
     */
    public function getOtherColoniesOptions(DataContainer $dc)
    {
        $varValue= array();

        //aktuelle Colonie zu Standortbestimmung
        $objCurrentColony = BkmColoniesModel::findByPk($dc->activeRecord->pid);

        //hole alle übrigen Völker zu diesem Standort
        $objOtherColonies = BkmColoniesModel::findBy(['tl_bkm_colonies.id != ?','pid=?','death!=?'],[$dc->activeRecord->pid, $objCurrentColony->pid, 1]);
        if(null === $objOtherColonies) return $varValue;

        while($objOtherColonies->next())
        {
            $objBeeHive = BkmBeehiveModel::findById($objOtherColonies->hive_number);
            $varValue[$objOtherColonies->id] = $objBeeHive->nr.' ('.$objBeeHive->notiz.')';
        }
        return $varValue;
    }


    public function setTypeChanges(DataContainer $dc){
        
        $type = $dc->activeRecord->entry_type;

        switch($type) {
            case 'item':
                break;
            case 'set_rating':
                $this->changeRatingDescription($dc,'rating');
                break;
            case 'change_location':
                $this->changeLocationDescription($dc,'change_location');
                $this->updateLocation($dc);
                break;
            case 'status':
                $this->updateStatus($dc);
                break;
            case 'set_feed':
                $this->changeFeedDescription($dc, 'feed');
                break;
            case 'set_frames':
                $this->changeFramesDescription($dc, 'frames');
                break;
            case 'set_material':
                $this->changeMaterialDescription($dc, 'material');
                break;
           case 'set_medication':
                $this->changeMedicationDescription($dc, 'medication');
                break;
        }
    }

    /**
     * 
     * @param DataContainer $dc
     */
    public function updateLocation(DataContainer $dc) {

        $newLocation = Input::post('new_location');

        if($dc->activeRecord->entry_type == 'change_location' && (int) $newLocation > 0)
        {

            // dem zugehoerigen Bienenstock den neuen Standort zuweisen
            $this->changeLocation($dc->activeRecord->pid, $newLocation);

            // wenn mehrere Bienenstoecke übernommen werden sollen
            $other_colonies = Input::post('other_colonies');

            if (Input::post('multi_entry') == 1 && count($other_colonies) > 0) {

                foreach ($other_colonies as $colony_id) {
                    $this->changeLocation($colony_id, $newLocation);
                }
            }
        }
    }

    /**
     * @param int $colony_id
     * @param int $newLocation
     */
    public function changeLocation($colony_id, $newLocation) {

        $objColony = BkmColoniesModel::findByPk((int)$colony_id);

        if(null !== $objColony && $newLocation > 0) {
            $objColony->pid = $newLocation;
            $objColony->save();
        }
    }

    /**
     * setzt bei jedem Speichern den Standort zum Stockkarteneintrag
     * @param DataContainer $dc
     */
    public function setLocationToHivemap(DataContainer $dc) {
        $objColony = BkmColoniesModel::findByPk((int) $dc->activeRecord->pid);
        if(null === $objColony) return;

        $objHivemap = BkmHivemapModel::findByPk((int) $dc->id);
        if(null === $objHivemap) return;

        $objHivemap->location = $objColony->pid;
        $objHivemap->save();
    }

    /**
     * setzte die Beschreibung für den Standortwechsel
     * @param DataContainer $dc
     * @param string $type
     */
    public function changeLocationDescription(DataContainer $dc, $type = '') {

        $location = Input::post('new_location');

        //wenn Beschreibung noch leer dann neuen Werte setzen
        if( strlen($dc->activeRecord->description) < 1 && strlen($type) > 0) {

            $objLocation = BkmLocationModel::findById($location);
            $dc->activeRecord->description = sprintf($GLOBALS['TL_LANG']['tl_bkm_hivemap']['change_description'][$type], $objLocation->name);

            $objHivemap = BkmHivemapModel::findByPk($dc->id);
            $objHivemap->description = $dc->activeRecord->description;
            $objHivemap->save();
        }
    }

    /**
     * setzte die Beschreibung für die Voelkerfuetterung
     * @param DataContainer $dc
     * @param string $type
     */
    public function changeFeedDescription(DataContainer $dc, $type = '') {

        $arrFeed = Input::post('feed');
        if(!is_array($arrFeed) || count($arrFeed) < 1) return;

        //wenn Beschreibung noch leer dann neuen Werte setzen
        if( strlen($dc->activeRecord->description) < 1 && strlen($type) > 0) {
            $arrDescr = [];
            foreach($arrFeed as $entry){
                $arrDescr[] = $entry['sign'].' '.$entry['count'].' '.$GLOBALS['TL_LANG']['tl_bkm_hivemap']['volumes_options'][$entry['volume']].' '.$GLOBALS['TL_LANG']['tl_bkm_hivemap']['feed_options'][$entry['feed_types']];
            }

            $dc->activeRecord->description = sprintf($GLOBALS['TL_LANG']['tl_bkm_hivemap']['change_description'][$type], implode(",",$arrDescr));

            $objHivemap = BkmHivemapModel::findByPk($dc->id);
            $objHivemap->description = $dc->activeRecord->description;
            $objHivemap->save();
        }
    }

    /**
     * setzte die Beschreibung für die Voelkerfuetterung
     * @param DataContainer $dc
     * @param string $type
     */
    public function changeFramesDescription(DataContainer $dc, $type = '') {

        $arrFrames = Input::post('frames');
        if(!is_array($arrFrames) || count($arrFrames) < 1) return;

        //wenn Beschreibung noch leer dann neuen Werte setzen
        if( strlen($dc->activeRecord->description) < 1 && strlen($type) > 0) {
            $arrDescr = [];
            foreach($arrFrames as $entry){
                $arrDescr[] = $entry['sign'].' '.$entry['count'].' '.$GLOBALS['TL_LANG']['tl_bkm_hivemap']['frametype_options'][$entry['frame_types']];
            }

            $dc->activeRecord->description = sprintf($GLOBALS['TL_LANG']['tl_bkm_hivemap']['change_description'][$type], implode(",",$arrDescr));

            $objHivemap = BkmHivemapModel::findByPk($dc->id);
            $objHivemap->description = $dc->activeRecord->description;
            $objHivemap->save();
        }
    }

    /**
     * setzte die Beschreibung für die Materialgabe
     * @param DataContainer $dc
     * @param string $type
     */
    public function changeMaterialDescription(DataContainer $dc, $type = '') {

        $arrMaterial = Input::post('material');
        if(!is_array($arrMaterial) || count($arrMaterial) < 1) return;

        //wenn Beschreibung noch leer dann neuen Werte setzen
        if( strlen($dc->activeRecord->description) < 1 && strlen($type) > 0) {
            $arrDescr = [];
            foreach($arrMaterial as $entry){
                $arrDescr[] = $entry['sign'].''.$entry['count'].' '.$entry['material_types'];
            }

            $dc->activeRecord->description = sprintf($GLOBALS['TL_LANG']['tl_bkm_hivemap']['change_description'][$type], implode(", ",$arrDescr));

            $objHivemap = BkmHivemapModel::findByPk($dc->id);
            $objHivemap->description = $dc->activeRecord->description;
            $objHivemap->save();
        }
    }

    /**
     * setzte die Beschreibung für die Medikamentengaben
     * @param DataContainer $dc
     * @param string $type
     */
    public function changeMedicationDescription(DataContainer $dc, $type = '') {

        $arrMedication = Input::post('medication');
        if(!is_array($arrMedication) || count($arrMedication) < 1) return;

        //wenn Beschreibung noch leer dann neuen Werte setzen
        if( strlen($dc->activeRecord->description) < 1 && strlen($type) > 0) {
            $arrDescr = [];
            foreach($arrMedication as $entry){
                $arrDescr[] = $entry['sign'].' '.$entry['count'].' '.$GLOBALS['TL_LANG']['tl_bkm_hivemap']['volumes_options'][$entry['volume']].' '.$GLOBALS['TL_LANG']['tl_bkm_hivemap']['meditype_options'][$entry['medi_types']];
            }

            $dc->activeRecord->description = sprintf($GLOBALS['TL_LANG']['tl_bkm_hivemap']['change_description'][$type], implode(",",$arrDescr));

            $objHivemap = BkmHivemapModel::findByPk($dc->id);
            $objHivemap->description = $dc->activeRecord->description;
            $objHivemap->save();
        }
    }


    /**
     * generiert den Beschreibungstext aus den Bewertungs-Selectfeldern
     * @param DataContainer $dc
     * @param string $type
     */
    public function changeRatingDescription(DataContainer $dc, $type = '') {

        //wenn Beschreibung noch leer dann neuen Werte setzen
        if( strlen($dc->activeRecord->description) < 1 && strlen($type) > 0) {
            $description = '';
            $arrDescr = [];

            if(strlen($dc->activeRecord->rating_breed) > 0) {
                $objBreed = BkmBreedQualityModel::findById($dc->activeRecord->rating_breed);
                if(null !== $objBreed) $arrDescr[] = sprintf($GLOBALS['TL_LANG']['tl_bkm_hivemap']['change_description'][$type]['breed_quality'], $objBreed->name);

            }

            if(strlen($dc->activeRecord->rating_feed) > 0) {
                $objFeed = BkmFeedQualityModel::findById($dc->activeRecord->rating_feed);
                if(null !== $objFeed) $arrDescr[] = sprintf($GLOBALS['TL_LANG']['tl_bkm_hivemap']['change_description'][$type]['feed_quality'], $objFeed->name);
            }

            if(strlen($dc->activeRecord->rating_gentleness) > 0) {
                $objGentleness = BkmBreedQualityModel::findById($dc->activeRecord->rating_gentleness);
                if(null !== $objGentleness) $arrDescr[] = sprintf($GLOBALS['TL_LANG']['tl_bkm_hivemap']['change_description'][$type]['gentleness'], $objGentleness->name);
            }

            if(strlen($dc->activeRecord->rating_population) > 0) {
                $objPopulation = BkmPopulationSizeModel::findById($dc->activeRecord->rating_population);
                if(null !== $objPopulation) $arrDescr[] = sprintf($GLOBALS['TL_LANG']['tl_bkm_hivemap']['change_description'][$type]['population'], $objPopulation->title);
            }

            if(is_array($arrDescr) && count($arrDescr) > 0) {
                $objHivemap = BkmHivemapModel::findByPk($dc->id);
                $objHivemap->description = implode('; ', $arrDescr);
                $objHivemap->save();
            }
        }
    }

    
    /**
     * @param DataContainer $dc
     */
    public function setCopyEntriesInOtherColonies(DataContainer $dc)
    {
        // wenn mehrere übernommen werden sollen
        $other_colonies = Input::post('other_colonies');

        //hole DB-Model z.B. um das kurz zuvor gesetzte location für die anderen Eintraege zusetzen
        $objCurrentHivemap = BkmHivemapModel::findByPk($dc->id);
        if(null === $objCurrentHivemap) return false;

        if ($dc->activeRecord && Input::post('multi_entry') == 1 && count($other_colonies) > 0) {

            foreach($other_colonies as $colony_id) {

                $objHivemap = BkmHivemapModel::findOneBy(['tl_bkm_hivemap.copy_from='.$dc->id, 'tl_bkm_hivemap.pid='.$colony_id],null);
                //Stockkarten eintrag neu anlegen falls er noch nicht existiert
                if(null === $objHivemap) $objHivemap = new BkmHivemapModel();

                $set = [
                    'pid' => $colony_id,
                    'tstamp' => time(),
                    'copy_from' => $dc->id,
                    'modify' => time(),
                    'entry_type' => $dc->activeRecord->entry_type,
                    'date' => $dc->activeRecord->date,
                    'description' => $objCurrentHivemap->description,
                    'location' => $objCurrentHivemap->location,
                    'status' => $dc->activeRecord->status,
                    'frames' => $dc->activeRecord->frames,
                    'medication' => $dc->activeRecord->medication,
                    'feed' => $dc->activeRecord->feed,
                    'rating_breed' => $dc->activeRecord->rating_breed,
                    'rating_feed' => $dc->activeRecord->rating_feed,
                    'rating_gentleness' => $dc->activeRecord->rating_gentleness,
                ];

                $objHivemap->setRow($set);
                $objHivemap->save();
            }
        }
    }

    public function updateStatus(DataContainer $dc) {

        $objColony = BkmColoniesModel::findByPk($dc->activeRecord->pid);
        if(!is_object($objColony)) return false;

        //descrion setzen gilt fuer alle egal welchen Typ

        //Statusmeldung als Beschreibungstext setzen
        $objHivemap = BkmHivemapModel::findByPk($dc->id);
        $objColony = BkmColoniesModel::findByPk($dc->activeRecord->pid);

        if(strlen($dc->activeRecord->description) < 1 && null !== $objHivemap) {
            $objHivemap->description = sprintf($GLOBALS['TL_LANG']['tl_bkm_hivemap']['change_description']['status'][$dc->activeRecord->status],$objColony->hive_number);
            $objHivemap->save();
        }

        switch($dc->activeRecord->status) {
            case 1: //erstellt
                break;
            case 2: //aufgelöst
            case 3: //tot

                //Bienenvolk als aufgelöst/tot setzen
                $objColony->death = 1;
                $objColony->save();

                //Bienenbeute auf unbenutzt setzen
                $objBeehive = BkmBeehiveModel::findByPk($objColony->hive_number);
                if(null !== $objBeehive){
                    $objBeehive->used_from = 0;
                    $objBeehive->status = '';
                    $objBeehive->save();
                }
                break;
        }
    }
    
    public function setAllLocationsToHivemap(DataContainer $dc){

        //alle Stockkarten holen
        $objHivemap = BkmHivemapModel::findAll();
        if(null === $objHivemap) return;

        while($objHivemap->next()){
            if((int) $objHivemap->location > 0) continue;

            $objColonies = BkmColoniesModel::findByPk($objHivemap->pid);
            if(null === $objColonies) continue;

            $objHivemap->location = $objColonies->pid;
            $objHivemap->save();
        }
    }
    
    public function debugHivemap(DataContainer $dc){
        /** @var Symfony\Component\HttpFoundation\Session\SessionInterface $objSession */
        $objSession = System::getContainer()->get('session');
        $objSessionBag = $objSession->getBag('contao_backend');
        dump($objSessionBag); die();
    }
}
