<?php
/**
 * Created by c4.pringitzhonig.de.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 08.09.19
 */

namespace Srhinow\BeekeepingManagerBundle\EventListener\Dca;


use Contao\Backend;
use Contao\Database;
use Contao\DataContainer;
use Contao\FilesModel;
use Srhinow\BeekeepingManagerBundle\Model\BkmBeeBreedModel;
use Srhinow\BeekeepingManagerBundle\Model\BkmBeehiveModel;

class BkmColonies extends Backend
{
    /**
     * Import the back end user object
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * List a particular record
     * @param array
     * @return string
     */
    public function listEntries($arrRow, $strLabel, $dc, $args)
    {
        if(strlen($args[0]) > 0){
            $image = '';
            $objImage = FilesModel::findByUuid($args[0]);
            if ($objImage !== null)
            {
                $fullpath = $objImage->path;
                $smallpath = \Image::getHtml(\Image::get($fullpath,0, 50,'proportional'),'','');
            }
            $args[0] = '<a href="'.$fullpath.'" target="_blank"  onclick="Backend.openModalIframe({\'title\':\'Bild vom Bienenvolk anzeigen.\',\'url\':this.href});return false">'.$smallpath.'</a>';
//            dump($smallpath); die();
        }

//        $objHive = BkmBeehiveModel::findById($arrRow['hive_number']);
//        $return = $objHive->nr.' ('.$GLOBALS['TL_LANG']['tl_bkm_colonies']['queen_color_options'][$arrRow['queen_color']].')';
//        if($arrRow['death']) $return .= ', aufgelöst';

        return $args;
    }
    /**
     * Return the edit Hive button
     * @param $row
     * @param $href
     * @param $label
     * @param $title
     * @param $icon
     * @param $attributes
     * @return string
     */
    public function hivemapLink($row, $href, $label, $title, $icon, $attributes)
    {
        return '<a href="'.$this->addToUrl($href.'&amp;id='.$row['id']).'" title="'.specialchars($title).'"'.$attributes.'>'.$this->generateImage($icon, $label).'</a> ';
    }

    /**
     * get all unused beehives
     * @param $dc
     * @return array
     */
    public function getHiveOptions(\DataContainer $dc)
    {
        $varValue= array();

        $objHive = Database::getInstance()->prepare('SELECT * FROM `tl_bkm_beehive` WHERE status = ? OR used_from= ? ORDER BY `sorting` ASC')
            ->execute(0 , $dc->id);


        while($objHive->next())
        {
            $varValue[$objHive->id] = $objHive->nr.' ('.$objHive->notiz.')';
        }

        return $varValue;
    }

    /**
     * holt sich alle Beinenstöcke ohne sich selbst und die die aufgelösst wurden als Abstammung-Optionen
     * @param \DataContainer $dc
     * @return array
     */
    public function getNativityOptions(DataContainer $dc)
    {
        $varValue= array();

        $all = Database::getInstance()->prepare('SELECT * FROM `tl_bkm_colonies` WHERE `id` !=? AND death != 1 ORDER BY `hive_number` ASC')
            ->execute($dc->id);

        while($all->next())
        {
            $varValue[$all->id] = $all->hive_number.' ('.$all->hive_notice.')';
        }
        return $varValue;
    }

    /**
     * @param \DataContainer $dc
     */
    public function setBeehiveStatus(DataContainer $dc)
    {

        //alle alten Einträge in Bienenstock zurücksetzen
        Database::getInstance()->prepare('UPDATE tl_bkm_beehive SET used_from = ? WHERE used_from = ?')->execute(0,$dc->id);

        if($dc->activeRecord->death > 0) {
            $set['used_from'] = 0;
            $set['status'] = '';
        } else {
            $set['used_from'] = $dc->id;
            $set['status'] = 1;
        }
        // neuen Eintrag in Bienenstock setzen
        Database::getInstance()->prepare('UPDATE tl_bkm_beehive %s WHERE id = ?')->set($set)->execute($dc->activeRecord->hive_number);

        // falls tot die Bienenstock-Zuweisung löschen
//        if($dc->activeRecord->death > 0) Database::getInstance()->prepare('UPDATE tl_bkm_colonies SET hive_number = ? WHERE id = ?')->execute(0,$dc->id);
    }


    /**
     * get options for item units
     * @param object
     * @return array
     */
    public function getBeeBreedOptions(DataContainer $dc)
    {
        $varValue= array();

        $all = BkmBeeBreedModel::findAll(['order'=>'sorting ASC']);

        while($all->next())
        {
            $varValue[$all->id] = $all->name;
            if(strlen($all->short) > 0) $varValue[$all->id] .= ' ('.$all->short.')';
        }
        return $varValue;
    }
}