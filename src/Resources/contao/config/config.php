<?php
/**
 * @copyright  Sven Rhinow 2018
 * @author     sr-tag Sven Rhinow Webentwicklung <http://www.sr-tag.de>
 * @package    beekeeping-manager-bundle
 * @license    LGPL
 * @filesource
 */

/**
 * beekeeping-manager-bundle Version
 */
@define('BKM_VERSION', '1.0');
@define('BKM_BUILD', '16');
@define('BKM_PATH','vendor/srhinow/beekeeping-manager-bundle');
@define('BKM_PUBLIC_FOLDER','bundles/srhinowbeekeepingmanager');
@define('LOCATION_API_KEY','9e4c2e3bd44a3b'); // http://locationiq.org
@define('OPENWEATHERMAP_API_KEY','0ed9e2f4f6b89977b656038daf7c8209');

/**
 * Add back end modules
 */

if(array_key_exists('beekeeping',$GLOBALS['BE_MOD'])) array_insert($GLOBALS['BE_MOD'], 1, array( 'beekeeping' => array() ) );

array_insert($GLOBALS['BE_MOD']['beekeeping'], 0, array(
    'bkm_location' => array
    (
        'tables'	=> array('tl_bkm_location','tl_bkm_colonies','tl_bkm_hivemap'),
        'csvExport' => array('srhinow.beekeeeping_manager.backend.export_controller', 'csvExport'),
        'pdfExport' => array('srhinow.beekeeeping_manager.backend.export_controller', 'pdfExport'),
    ),
    'bkm_hivemap' => array
    (
        'tables'	=> array('tl_bkm_hivemap'),
    ),
    'bkm_material' => array
    (
        'tables'	=> array('tl_bkm_material'),
    ),
    'bkm_weather' => array
    (
        'callback'	=> 'Srhinow\BeekeepingManagerBundle\Modules\Be\ModuleBkmWeather'
    ),
    'bkm_settings' => array
		(
			'callback'	=> 'Srhinow\BeekeepingManagerBundle\Modules\Be\ModuleBkmSetup',
			'tables' => array(),
			'stylesheet' => BKM_PUBLIC_FOLDER.'/css/be.css',
		)
	)
);

/**
 * beekeeper management Modules
 */
$GLOBALS['BKM_MOD'] = array
(
	'bkm_properties' => array
	(
        'bkm_option_settings' => array
        (
            'tables' => array('tl_bkm_option_settings'),
            'icon'   => BKM_PUBLIC_FOLDER.'/icons/accordion.png',
        ),
	    'bkm_beehive' => array
        (
            'tables' => array('tl_bkm_beehive','tl_bkm_colonies','tl_bkm_hivemap'),
            'icon'   => BKM_PUBLIC_FOLDER.'/icons/caution_board.png',
        ),
		'bkm_frame_dimensions' => array
		(
			'tables'	=> array('tl_bkm_frame_dimensions'),
			'icon'      => BKM_PUBLIC_FOLDER.'/icons/picture_frame.png',
		),
	),
    'bkm_categories' => [
        'bkm_material_category' => [
            'tables'	=> array('tl_bkm_material_category'),
            'icon'      => BKM_PUBLIC_FOLDER.'/icons/forklift.png',
        ]
    ]
);

if ('BE' === TL_MODE) {
    $GLOBALS['TL_CSS'][] = BKM_PUBLIC_FOLDER.'/css/be.css|static';
}

// Enable tables in iao_setup
if (isset($_GET['do']) && $_GET['do'] == 'bkm_settings')
{
	foreach ($GLOBALS['BKM_MOD'] as $strGroup=>$arrModules)
	{
		foreach ($arrModules as $strModule => $arrConfig)
		{
			if (is_array($arrConfig['tables']))
			{
				$GLOBALS['BE_MOD']['beekeeping']['bkm_settings']['tables'] = array_merge($GLOBALS['BE_MOD']['beekeeping']['bkm_settings']['tables'], $arrConfig['tables']);
			}
		}
	}
}

/**
 * Models
 */
$GLOBALS['TL_MODELS']['tl_bkm_beehive'] = \Srhinow\BeekeepingManagerBundle\Model\BkmBeehiveModel::class;
$GLOBALS['TL_MODELS']['tl_bkm_colonies'] = \Srhinow\BeekeepingManagerBundle\Model\BkmColoniesModel::class;
$GLOBALS['TL_MODELS']['tl_bkm_frame_dimensions'] = \Srhinow\BeekeepingManagerBundle\Model\BkmFrameDimensionsModel::class;
$GLOBALS['TL_MODELS']['tl_bkm_hivemap'] = \Srhinow\BeekeepingManagerBundle\Model\BkmHivemapModel::class;
$GLOBALS['TL_MODELS']['tl_bkm_location'] = \Srhinow\BeekeepingManagerBundle\Model\BkmLocationModel::class;
$GLOBALS['TL_MODELS']['tl_bkm_weather_forcast'] = \Srhinow\BeekeepingManagerBundle\Model\BkmWeatherForcastModel::class;
$GLOBALS['TL_MODELS']['tl_bkm_material'] = \Srhinow\BeekeepingManagerBundle\Model\BkmMaterialModel::class;
$GLOBALS['TL_MODELS']['tl_bkm_material_category'] = \Srhinow\BeekeepingManagerBundle\Model\BkmMaterialCategoryModel::class;
$GLOBALS['TL_MODELS']['tl_bkm_option_settings'] = \Srhinow\BeekeepingManagerBundle\Model\BkmOptionSettingsModel::class;

/**
 * Frontend modules
 */
// $GLOBALS['FE_MOD']['beekeeping_colonies'] = array('bk_colonies' => 'ModuleColonyTable');

/**
 * HOOKS
 */
// $GLOBALS['TL_HOOKS']['replaceInsertTags'][] = array('mpMembers', 'changeIAOTags');
