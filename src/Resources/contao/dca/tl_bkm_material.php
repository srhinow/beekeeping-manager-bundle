<?php
/**
 * @copyright  Sven Rhinow 2019
 * @author     sr-tag Sven Rhinow Webentwicklung <http://www.sr-tag.de>
 * @package    beekeeping-manager-bundle
 * @license    LGPL
 * @filesource
 */

/**
 *Table tl_bkm_material
 */
$GLOBALS['TL_DCA']['tl_bkm_material'] = array
(
    //Config
    'config' => array
    (
        'dataContainer' => 'Table',
        'enableVersioning'            => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
            )
        )
    ),
    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode'                    => 1,
            'fields'                  => array('category'),
            'flag'					  => 1,
        ),
        'label' => array
        (
            'fields'                  => array('count','title','category:tl_bkm_material_category.title'),
            'format'                  => '%s',
            'showColumns'             => true,
        ),
        'global_operations' => array
        (
            'back' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['MSC']['backBT'],
                'href'                => 'mod=&table=',
                'class'               => 'header_back',
                'attributes'          => 'onclick="Backend.getScrollOffset();"',
            ),
            'all' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href'                => 'act=select',
                'class'               => 'header_edit_all',
                'attributes'          => 'onclick="Backend.getScrollOffset();"',
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_bkm_material']['edit'],
                'href'                => 'act=edit',
                'icon'                => 'edit.gif',
            ),
            'copy' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_bkm_material']['copy'],
                'href'                => 'act=copy',
                'icon'                => 'copy.gif',
            ),
            'delete' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_bkm_material']['delete'],
                'href'                => 'act=delete',
                'icon'                => 'delete.gif',
                'attributes'          => 'onclick="if (!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\')) return false; Backend.getScrollOffset();"',
            ),
            'show' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_bkm_material']['show'],
                'href'                => 'act=show',
                'icon'                => 'show.gif',
            )
        )
    ),
    // Palettes
    'palettes' => array
    (
        'default' => 'title,category,count,notice;{expert_legend:hide},sorting'
    ),
    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'modify' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'title' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_material']['title'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'count' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_material']['count'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('rgxp'=> 'digit', 'mandatory'=>true, 'maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "int(10) unsigned NOT NULL default '0'",
        ),
        'category' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_material']['category'],
            'flag'                    => 1,
            'inputType'               => 'select',
            'foreignKey'              => 'tl_bkm_material_category.title',
            'eval'                    => array('multiple'=>false, 'includeBlankOption'=>true,'chosen' => true, 'tl_class'=>'w50'),
            'sql'                     => "int(10) unsigned NOT NULL default '0'",
            'relation'                => array('type'=>'belongsTo', 'load'=>'eager')
        ),
        'notice' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_material']['notice'],
            'exclude'                 => true,
            'search'				  => true,
            'filter'                  => false,
            'inputType'               => 'textarea',
            'eval'                    => array('mandatory'=>false, 'cols'=>'10','rows'=>'10','style'=>'height:100px','rte'=>false, 'tl_class'=>'clr'),
            'sql'                     => "text NULL"
        ),
        'sorting' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_material']['sorting'],
            'inputType'               => 'text',
            'eval'                    => array('tl_class'=>'w50'),
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),

    )
);
