<?php

/**
 * @copyright  Sven Rhinow 2018
 * @author     sr-tag Sven Rhinow Webentwicklung <http://www.sr-tag.de>
 * @package    beekeeping-manager-bundle
 * @license    LGPL
 * @filesource
 */

/**
*Table tl_bkm_option_settings
*/


$GLOBALS['TL_DCA']['tl_bkm_option_settings'] = array
(
	//Config
	'config' => array
	(
		'dataContainer' => 'Table',
		'enableVersioning'            => true,
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary'
			)
		),
        'oncreate_callback' => [
            array('srhinow.beekeepingmanager.listener.dca.bkm_option_settings', 'createDefaultValues'),
        ],
        'onload_callback' => [
            array('srhinow.beekeepingmanager.listener.dca.bkm_option_settings', 'setDefaultValues'),
        ],
	),
	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 1,
			'fields'                  => array('name'),
			'flag'                    => 1,
			'panelLayout'             => 'filter;search,limit'
		),
		'label' => array
		(
			'fields'                  => array('name','short'),
			'format'                  => '%s, (%s)',
		),
		'global_operations' => array
		(
			'back' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['backBT'],
				'href'                => 'mod=&table=',
				'class'               => 'header_back',
				'attributes'          => 'onclick="Backend.getScrollOffset();"',
			),
			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset();"',
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bkm_option_settings']['edit'],
				'href'                => 'act=edit',
				'icon'                => 'edit.gif',
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bkm_option_settings']['copy'],
				'href'                => 'act=copy',
				'icon'                => 'copy.gif',
			),
			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bkm_option_settings']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.gif',
				'attributes'          => 'onclick="if (!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\')) return false; Backend.getScrollOffset();"',
			),
			'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bkm_option_settings']['show'],
				'href'                => 'act=show',
				'icon'                => 'show.gif',
			)
		)
	),
	// Palettes
	'palettes' => array
	(
		'default' => 'name,fallback;
		{options_legend},material_options,status_options,frametype_options,meditype_options,volumes_options,feed_options,gentleness_options,population_size_options,feed_quantity_options,breed_quantity_options,bee_race_options'
	),
	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
		'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'modify' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
        'name' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_option_settings']['name'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'unique'=>true, 'maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'fallback' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_option_settings']['fallback'],
            'exclude'                 => true,
            'filter'                  => true,
            'inputType'               => 'checkbox',
            'eval'					  => array('doNotCopy'=>true, 'fallback'=>true, 'unique'=>true,'tl_class'=>'w50 m12'),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'status_options' =>  array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_option_settings']['status_options'],
            'exclude'                 => true,
            'inputType'               => 'multiColumnWizard',
            'eval' => array(
                'doNotCopy'=>true,
                'columnFields' => array
                (
                    'key' => array
                    (
                        'label'             => $GLOBALS['TL_LANG']['tl_bkm_option_settings']['key'],
                        'inputType'         => 'text',
                        'eval'              => array('style' => 'width:80px'),
                        'default'			=> ''
                    ),
                    'text' => array
                    (
                        'label'             => $GLOBALS['TL_LANG']['tl_bkm_option_settings']['text'],
                        'inputType'         => 'text',
                        'eval'              => array('style' => 'width:350px;'),
                    )
                )
            ),
            'sql'				=> "blob NULL"
        ),
        
        'frametype_options' =>  array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_option_settings']['frametype_options'],
            'exclude'                 => true,
            'inputType'               => 'multiColumnWizard',
            'eval' => array(
                'doNotCopy'=>true,
                'columnFields' => array
                (
                    'key' => array
                    (
                        'label'             => $GLOBALS['TL_LANG']['tl_bkm_option_settings']['key'],
                        'inputType'         => 'text',
                        'eval'              => array('style' => 'width:80px'),
                        'default'			=> ''
                    ),
                    'text' => array
                    (
                        'label'             => $GLOBALS['TL_LANG']['tl_bkm_option_settings']['text'],
                        'inputType'         => 'text',
                        'eval'              => array('style' => 'width:350px;'),
                    )
                )
            ),
            'sql'				=> "blob NULL"
        ),
        
        'meditype_options' =>  array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_option_settings']['meditype_options'],
            'exclude'                 => true,
            'inputType'               => 'multiColumnWizard',
            'eval' => array(
                'doNotCopy'=>true,
                'columnFields' => array
                (
                    'key' => array
                    (
                        'label'             => $GLOBALS['TL_LANG']['tl_bkm_option_settings']['key'],
                        'inputType'         => 'text',
                        'eval'              => array('style' => 'width:80px'),
                        'default'			=> ''
                    ),
                    'text' => array
                    (
                        'label'             => $GLOBALS['TL_LANG']['tl_bkm_option_settings']['text'],
                        'inputType'         => 'text',
                        'eval'              => array('style' => 'width:350px;'),
                    )
                )
            ),
            'sql'				=> "blob NULL"
        ),

        'volumes_options' =>  array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_option_settings']['volumes_options'],
            'exclude'                 => true,
            'inputType'               => 'multiColumnWizard',
            'eval' => array(
                'doNotCopy'=>true,
                'columnFields' => array
                (
                    'key' => array
                    (
                        'label'             => $GLOBALS['TL_LANG']['tl_bkm_option_settings']['key'],
                        'inputType'         => 'text',
                        'eval'              => array('style' => 'width:80px'),
                        'default'			=> ''
                    ),
                    'text' => array
                    (
                        'label'             => $GLOBALS['TL_LANG']['tl_bkm_option_settings']['text'],
                        'inputType'         => 'text',
                        'eval'              => array('style' => 'width:350px;'),
                    )
                )
            ),
            'sql'				=> "blob NULL"
        ),

        'bee_race_options' =>  array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_option_settings']['bee_race_options'],
            'exclude'                 => true,
            'inputType'               => 'multiColumnWizard',
            'eval' => array(
                'doNotCopy'=>true,
                'columnFields' => array
                (
                    'key' => array
                    (
                        'label'             => $GLOBALS['TL_LANG']['tl_bkm_option_settings']['key'],
                        'inputType'         => 'text',
                        'eval'              => array('style' => 'width:80px'),
                        'default'			=> ''
                    ),
                    'text' => array
                    (
                        'label'             => $GLOBALS['TL_LANG']['tl_bkm_option_settings']['text'],
                        'inputType'         => 'text',
                        'eval'              => array('style' => 'width:350px;'),
                    )
                )
            ),
            'sql'				=> "blob NULL"
        ),
        'breed_quantity_options' =>  array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_option_settings']['breed_quantity_options'],
            'exclude'                 => true,
            'inputType'               => 'multiColumnWizard',
            'eval' => array(
                'doNotCopy'=>true,
                'columnFields' => array
                (
                    'key' => array
                    (
                        'label'             => $GLOBALS['TL_LANG']['tl_bkm_option_settings']['key'],
                        'inputType'         => 'text',
                        'eval'              => array('style' => 'width:80px'),
                        'default'			=> ''
                    ),
                    'text' => array
                    (
                        'label'             => $GLOBALS['TL_LANG']['tl_bkm_option_settings']['text'],
                        'inputType'         => 'text',
                        'eval'              => array('style' => 'width:350px;'),
                    )
                )
            ),
            'sql'				=> "blob NULL"
        ),

        'feed_quantity_options' =>  array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_option_settings']['feed_quantity_options'],
            'exclude'                 => true,
            'inputType'               => 'multiColumnWizard',
            'eval' => array(
                'doNotCopy'=>true,
                'columnFields' => array
                (
                    'key' => array
                    (
                        'label'             => $GLOBALS['TL_LANG']['tl_bkm_option_settings']['key'],
                        'inputType'         => 'text',
                        'eval'              => array('style' => 'width:80px'),
                        'default'			=> ''
                    ),
                    'text' => array
                    (
                        'label'             => $GLOBALS['TL_LANG']['tl_bkm_option_settings']['text'],
                        'inputType'         => 'text',
                        'eval'              => array('style' => 'width:350px;'),
                    )
                )
            ),
            'sql'				=> "blob NULL"
        ),
        'population_size_options' =>  array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_option_settings']['population_size_options'],
            'exclude'                 => true,
            'inputType'               => 'multiColumnWizard',
            'eval' => array(
                'doNotCopy'=>true,
                'columnFields' => array
                (
                    'key' => array
                    (
                        'label'             => $GLOBALS['TL_LANG']['tl_bkm_option_settings']['key'],
                        'inputType'         => 'text',
                        'eval'              => array('style' => 'width:80px'),
                        'default'			=> ''
                    ),
                    'text' => array
                    (
                        'label'             => $GLOBALS['TL_LANG']['tl_bkm_option_settings']['text'],
                        'inputType'         => 'text',
                        'eval'              => array('style' => 'width:350px;'),
                    )
                )
            ),
            'sql'				=> "blob NULL"
        ),
        'gentleness_options' =>  array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_option_settings']['gentleness_options'],
            'exclude'                 => true,
            'inputType'               => 'multiColumnWizard',
            'eval' => array(
                'doNotCopy'=>true,
                'columnFields' => array
                (
                    'key' => array
                    (
                        'label'             => $GLOBALS['TL_LANG']['tl_bkm_option_settings']['key'],
                        'inputType'         => 'text',
                        'eval'              => array('style' => 'width:80px'),
                        'default'			=> ''
                    ),
                    'text' => array
                    (
                        'label'             => $GLOBALS['TL_LANG']['tl_bkm_option_settings']['text'],
                        'inputType'         => 'text',
                        'eval'              => array('style' => 'width:350px;'),
                    )
                )
            ),
            'sql'				=> "blob NULL"
        ),


        'feed_options' =>  array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_option_settings']['feed_options'],
            'exclude'                 => true,
            'inputType'               => 'multiColumnWizard',
            'eval' => array(
                'doNotCopy'=>true,
                'columnFields' => array
                (
                    'key' => array
                    (
                        'label'             => $GLOBALS['TL_LANG']['tl_bkm_option_settings']['key'],
                        'inputType'         => 'text',
                        'eval'              => array('style' => 'width:80px'),
                        'default'			=> ''
                    ),
                    'text' => array
                    (
                        'label'             => $GLOBALS['TL_LANG']['tl_bkm_option_settings']['text'],
                        'inputType'         => 'text',
                        'eval'              => array('style' => 'width:350px;'),
                    )
                )
            ),
            'sql'				=> "blob NULL"
        ),
        'material_options' =>  array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_option_settings']['material_options'],
            'exclude'                 => true,
            'inputType'               => 'multiColumnWizard',
            'eval' => array(
                // 'style'                 => 'width:100%;',
                'doNotCopy'=>true,
                'columnFields' => array
                (
                    'key' => array
                    (
                        'label'             => $GLOBALS['TL_LANG']['tl_bkm_option_settings']['key'],
                        'inputType'         => 'text',
                        'eval'              => array('style' => 'width:80px'),
                        'default'			=> ''
                    ),
                    'text' => array
                    (
                        'label'             => $GLOBALS['TL_LANG']['tl_bkm_option_settings']['text'],
                        'inputType'         => 'text',
                        'eval'              => array('style' => 'width:300px;'),
                    ),
                    'material' => array
                    (
                        'label'             => $GLOBALS['TL_LANG']['tl_bkm_option_settings']['material'],
                        'inputType'         => 'select',
                        'options_callback'  => array('srhinow.beekeepingmanager.listener.dca.bkm_option_settings', 'getMaterialOptionsForSettings'),
                        'eval'              => array( 'tl_class'=>'wizard','style' => 'width:250px','chosen'=>true , 'includeBlankOption' => true),
                    ),
                )
            ),
            'sql'				=> "blob NULL"
        ),
	)
);