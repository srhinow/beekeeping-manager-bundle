<?php
/**
 * @copyright  Sven Rhinow 2018
 * @author     sr-tag Sven Rhinow Webentwicklung <http://www.sr-tag.de>
 * @package    beekeeping-manager-bundle
 * @license    LGPL
 * @filesource
 */

/**
*Table tl_bkm_colonies
*/

$GLOBALS['TL_DCA']['tl_bkm_colonies'] = array
(
	//Config
	'config' => array
	(
		'dataContainer' => 'Table',
        'ptable'                      => 'tl_bkm_location',
		'ctable'                      => array('tl_bkm_hivemap'),
        'doNotCopyRecords'			  => true,
        'switchToEdit'                => true,
        'enableVersioning'            => false,
        'onsubmit_callback' => array
        (
            array('srhinow.beekeepingmanager.listener.dca.bkm_colonies', 'setBeehiveStatus')
        ),
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary',
                'pid' => 'index'
			)
		)
	),
	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 2,
			'fields'                  => array('date DESC'),
			'panelLayout'             => 'filter;sort,search,limit'
		),
		'label' => array
		(
			'fields'                  => array('image','hive_number:tl_bkm_beehive.nr', 'queen_color','stand_number'),
			'format'                  => '<span style="font-weight: bold;">%s (%s)</span>, aufgelöst: %s',
            'showColumns'             => true,
 			'label_callback'          => array('srhinow.beekeepingmanager.listener.dca.bkm_colonies', 'listEntries')
		),
		'global_operations' => array
		(
			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
					'href'                => 'act=select',
					'class'               => 'header_edit_all',
					'attributes'          => 'onclick="Backend.getScrollOffset();"'
			)
		),
		'operations' => array
		(
			'hivemap' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bkm_colonies']['hivemap'],
				'href'                => 'table=tl_bkm_hivemap&only=colony',
				'icon'                => BKM_PUBLIC_FOLDER.'/icons/table_18.png',
				'button_callback'     => array('srhinow.beekeepingmanager.listener.dca.bkm_colonies', 'hivemapLink')
			),
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bkm_colonies']['edit'],
				'href'                => 'act=edit',
				'icon'                => 'header.gif'
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bkm_colonies']['copy'],
				'href'                => 'act=copy',
				'icon'                => 'copy.gif'
			),
			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bkm_colonies']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.gif',
				'attributes'          => 'onclick="if (!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\')) return false; Backend.getScrollOffset();"'
			),
			'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bkm_colonies']['show'],
				'href'                => 'act=show',
				'icon'                => 'show.gif'
			)
		)
	),
	// Palettes
	'palettes' => array
	(
		 '__selector__'                => array('addImage'),
		'default'                     => 'date,pid;{image_legend},addImage;{hive_legend},stand_number,hive_number,hive_notice;{breed_legend},bee_race,breed_notice,queen_color,nativity_id,nativity;{additional_legend},notice,death,attention'
	),
	// Subpalettes
	'subpalettes' => array
	(
		 'addImage'                            => 'image'
	),
	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
        'pid' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_colonies']['pid'],
            'foreignKey'              => 'tl_bkm_location.name',
            'filter'                  => true,
            'sorting'                 => true,
            'flag'                    => 11,
            'inputType'               => 'select',
            'eval'                    => array('tl_class'=>'w50','includeBlankOption'=>false, 'chosen'=>true),
            'sql'                     => "int(10) unsigned NOT NULL default '0'",
            'relation'                => array('type'=>'belongsTo', 'load'=>'eager')
        ),
        'main_site' => array
        (
            'sql'                     => "varchar(32) NOT NULL default ''"
        ),
		'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'modify' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'sorting' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'date' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_colonies']['date'],
			'inputType'               => 'text',
			'default'                 => time(),
			'search'                  => true,
			'sorting'                 => true,
			'flag'                    => 6,
			'eval'                    => array('mandatory'=>true, 'datepicker'=>$this->getDatePickerString(), 'tl_class'=>'wizard w50', 'minlength' => 1, 'maxlength'=>10, 'rgxp' => 'date'),
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'hive_number' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_colonies']['hive_number'],
			'filter'                  => true,
			'sorting'                 => true,
			'flag'                    => 11,
			'inputType'               => 'select',
            'foreignKey'              => 'tl_bkm_beehive.nr',
            'options_callback'        => array('srhinow.beekeepingmanager.listener.dca.bkm_colonies', 'getHiveOptions'),
			'eval'                    => array('mandatory'=>false, 'chosen'=>true, 'includeBlankOption'=>true, 'tl_class'=>'w50'),
			'sql'                     => "int(10) unsigned NOT NULL default '0'",
		),
        'stand_number' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_colonies']['stand_number'],
            'search'                  => true,
            'sorting'                 => true,
            'flag'                    => 1,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>false, 'tl_class'=>'w50'),
            'sql'                     => "int(10) unsigned NOT NULL default '0'",
        ),
		'hive_notice' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_colonies']['hive_notice'],
			'search'                  => true,
			'sorting'                 => true,
			'flag'                    => 1,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>false, 'maxlength'=>255, 'tl_class'=>'w50'),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'bee_race' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_colonies']['bee_race'],
			'filter'                  => true,
			'sorting'                 => true,
			'flag'                    => 11,
			'inputType'               => 'select',
            'options_callback'        => array('srhinow.beekeepingmanager.listener.dca.bkm_option_settings', 'getBeeRaceOptions'),
			'eval'                    => array('mandatory'=>false, 'maxlength'=>255, 'tl_class'=>'w50', 'includeBlankOption'=>false,'submitOnChange'=>false),
			'sql'                     => "varchar(32) NOT NULL default ''",
		),
		'breed_notice' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_colonies']['breed'],
			'search'                  => true,
			'sorting'                 => true,
			'flag'                    => 1,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>false, 'maxlength'=>255, 'tl_class'=>'w50'),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'nativity_id' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_colonies']['nativity_id'],
			'filter'                  => true,
			'sorting'                 => true,
			'flag'                    => 11,
			'inputType'               => 'select',
			'options_callback'        => array('srhinow.beekeepingmanager.listener.dca.bkm_colonies', 'getNativityOptions'),
			'eval'                    => array('includeBlankOption'=>true, 'chosen'=>true, 'tl_class'=>'w50'),
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'nativity' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_colonies']['nativity'],
			'search'                  => true,
			'sorting'                 => true,
			'flag'                    => 1,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>false, 'maxlength'=>255, 'tl_class'=>'clr long'),
			'sql'                     => "varchar(128) NOT NULL default ''"
		),
        'queen_color' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_colonies']['queen_color'],
            'filter'                  => true,
            'sorting'                 => true,
            'flag'                    => 11,
            'inputType'               => 'select',
            'options'                 => $GLOBALS['TL_LANG']['tl_bkm_colonies']['queen_color_options'],
            'eval'                    => array('mandatory'=>false, 'maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(25) NOT NULL default ''"
        ),
		'notice' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_colonies']['notice'],
			'exclude'                 => true,
			'search'				  => true,
			'filter'                  => false,
			'inputType'               => 'textarea',
			'eval'                    => array('mandatory'=>false, 'cols'=>'10','rows'=>'10','style'=>'height:100px','rte'=>false),
			'sql'                     => "text NULL"
		),
		'death' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_colonies']['death'],
			'exclude'                 => true,
			'filter'                  => true,
			'sorting'                 => true,
			'inputType'               => 'checkbox',
			'flag'                    => 11,
			'eval'                    => array('doNotCopy'=>true),
			'sql'                     => "char(1) NOT NULL default ''"
		),
        'addImage' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_colonies']['addImage'],
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('submitOnChange'=>true),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'image' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_colonies']['image'],
            'exclude'                 => true,
            'inputType'               => 'fileTree',
            'eval'                    => array('filesOnly'=>true, 'fieldType'=>'radio', 'mandatory'=>true, 'tl_class'=>'clr'),
            'sql'                     => "binary(16) NULL"
        ),
        'attention' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_colonies']['attention'],
            'exclude'                 => true,
            'filter'                  => true,
            'sorting'                 => true,
            'inputType'               => 'checkbox',
            'flag'                    => 11,
            'eval'                    => array('doNotCopy'=>true),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
    )
);

