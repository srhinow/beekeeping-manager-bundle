<?php
/**
 * @copyright  Sven Rhinow 2018
 * @author     sr-tag Sven Rhinow Webentwicklung <http://www.sr-tag.de>
 * @package    beekeeping-manager-bundle
 * @license    LGPL
 * @filesource
 */

/**
 * Table tl_bkm_hivemaps
 */
$GLOBALS['TL_DCA']['tl_bkm_hivemap'] = array
(

	// Config
	'config' => array
	(
		'dataContainer'             => 'Table',
		'ptable'                    => 'tl_bkm_colonies',
		'switchToEdit'                => true,
		'enableVersioning'            => true,
		'sql' => array
		(
			'keys' => array
			(
                'id' => 'primary',
                'pid' => 'index'
			)
		),
		'onload_callback' => [
            array('srhinow.beekeepingmanager.listener.dca.bkm_hivemap', 'setPallete'),
        ],
		'onsubmit_callback' => [
            array('srhinow.beekeepingmanager.listener.dca.bkm_hivemap', 'setTypeChanges'),
            array('srhinow.beekeepingmanager.listener.dca.bkm_hivemap', 'setLocationToHivemap'),
		    array('srhinow.beekeepingmanager.listener.dca.bkm_hivemap','setCopyEntriesInOtherColonies'),
        ]

	),

	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 2,
            'flag'     => 1,
            'panelLayout'             => 'filter;sort,search,limit',
			'fields'                  => array('date DESC'),
			'headerFields'            => array('hive_number', 'entry_type', 'main_site'),
            'child_record_callback'   => array('srhinow.beekeepingmanager.listener.dca.bkm_hivemap', 'listItems'),
		),
		'label' => array
		(
			'fields'                  => array('date','entry_type', 'description','pid','location:tl_bkm_location.name'),
			'format'                  => '%s: %s',
            'showColumns'             => true,
            'label_callback'   => array('srhinow.beekeepingmanager.listener.dca.bkm_hivemap', 'labelGroupCallback')
		),
		'global_operations' => array
		(
		    // ToDo: PDF und CSV-Export lauffähig machen
			'pdfExport' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['pdfExport'],
				'href'                => 'key=pdfExport',
				'class'               => 'pdf_export',
//				'attributes'          => 'onclick="Backend.getScrollOffset();"'
			),
			'csvExport' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['csvExport'],
				'href'                => 'key=csvExport',
				'class'               => 'csv_export',
//				'attributes'          => 'onclick="Backend.getScrollOffset();"'
			),
			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['edit'],
				'href'                => 'act=edit',
				'icon'                => 'edit.gif'
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['copy'],
				'href'                => 'act=copy',
				'icon'                => 'copy.gif'
			),
			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.gif',
				'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
			),
			'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['show'],
				'href'                => 'act=show',
				'icon'                => 'show.gif'
			)
		)
	),

	// Palettes
	'palettes' => array
	(
		'__selector__'               => array('entry_type','multi_entry'),
		'default'                    => 'entry_type,date;{basic_legend},description;{multi_entry_legend},multi_entry',
		'set_rating'                 => 'entry_type,date;{rating_legend},rating_breed,rating_feed,rating_gentleness,rating_population,description;{multi_entry_legend},multi_entry',
		'set_feed'                   => 'entry_type,date;{feed_legend},feed,description;{multi_entry_legend},multi_entry',
		'set_frames'                 => 'entry_type,date;{frame_legend},frames,description;{multi_entry_legend},multi_entry',
		'set_material'               => 'entry_type,date;{frame_legend},material,description;{multi_entry_legend},multi_entry',
		'set_medication'             => 'entry_type,date;{medication_legend},medication,description;{multi_entry_legend},multi_entry',
		'change_location'            => 'entry_type,date;{change_location_legend},new_location,description;{multi_entry_legend},multi_entry',
		'status'                     => 'entry_type,date;{basic_legend},status,description;{multi_entry_legend},multi_entry',
	),
    // Subpalettes
    'subpalettes' => array
    (
        'multi_entry' => ('other_colonies')
    ),
	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
		'pid' => array
		(
            'label'                 => &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['pid'],
            'inputType'             => 'select',
            'foreignKey'            => 'tl_bkm_colonies.hive_number',
            'filter'                => true,
            'sorting'               => true,
            'options_callback'        => array('srhinow.beekeepingmanager.listener.dca.bkm_hivemap', 'getFilteredColoniesOptions'),
            'eval'                  => array('tl_class'=>'w50', 'chosen'=>true),
            'sql'                   => "int(10) unsigned NOT NULL default '0'",
		),
		'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'sorting' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'copy_from' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'modify' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'entry_type' => array
		(
			'label'                 => &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['entry_type'],
			'default'               => 'item',
			'exclude'               => true,
			'filter'                => true,
			'inputType'             => 'select',
			'options' 		  		=> &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['type_options'],
			'eval'                  => array( 'submitOnChange'=>true,'tl_class'=>'w50'),
			'sql'					=> "varchar(32) NOT NULL default 'item'"
		),
		'multi_entry' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['multi_entry'],
            'filter'                  => true,
            'flag'                    => 1,
            'inputType'               => 'checkbox',
            'eval'                    => array('doNotCopy'=>true,'submitOnChange'=>true),
            'sql'					  => "char(1) NOT NULL default ''"
        ),
        'other_colonies' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['other_colonies'],
            'flag'                    => 1,
            'inputType'               => 'checkboxWizard',
            'foreignKey'              => 'tl_bkm_colonies.hive_number',
            'options_callback'        => array('srhinow.beekeepingmanager.listener.dca.bkm_hivemap', 'getOtherColoniesOptions'),
            'eval'                    => array('multiple'=>true),
            'sql'                     => "blob NULL",
            'relation'                => array('type'=>'belongsToMany', 'load'=>'lazy')
        ),
		'date' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['date'],
			'inputType'               => 'text',
			'default'				  => time(),
			'exclude'                 => true,
			'filter'                  => true,
			'sorting'                 => true,
			'flag'                    => 8,
			'eval'                    => array('mandatory'=>true, 'datepicker'=>$this->getDatePickerString(), 'tl_class'=>'wizard w50', 'minlength' => 1, 'maxlength'=>10, 'rgxp' => 'date'),
			'sql'                     => "varchar(10) NOT NULL default ''"
		),
		'description' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['description'],
			'exclude'                 => true,
			'search'				  => true,
			'filter'                  => false,
			'inputType'               => 'textarea',
			'eval'                    => array('mandatory'=>false, 'cols'=>'10','rows'=>'10','style'=>'height:100px','rte'=>false, 'helpwizard'=>true,'tl_class'=>'clr long'),
			'explanation'             => 'shortCuts',
			'sql'                     => "text NULL"
		),
		'location' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['location'],
			'foreignKey'              => 'tl_bkm_location.name',
			'filter'                  => true,
			'sorting'                 => true,
			'flag'                    => 11,
			'inputType'               => 'select',
			'eval'                    => array('mandatory'=>false, 'maxlength'=>255, 'tl_class'=>'w50', 'includeBlankOption'=>false,'submitOnChange'=>false),
			'sql'                     => "int(10) unsigned NOT NULL default '0'",
			'relation'                => array('type'=>'belongsTo', 'load'=>'eager')
		),
        'new_location' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['new_location'],
            'foreignKey'              => 'tl_bkm_location.name',
            'flag'                    => 11,
            'inputType'               => 'select',
            'eval'                    => array('mandatory'=>false, 'maxlength'=>255, 'tl_class'=>'w50', 'includeBlankOption'=>false,'submitOnChange'=>false),
            'sql'                     => "int(10) unsigned NOT NULL default '0'",
            'relation'                => array('type'=>'belongsTo', 'load'=>'eager')
        ),
		'status' => array
		(
			'label'                 => &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['status'],
			'default'               => 'item',
			'exclude'               => true,
			'filter'                => true,
			'inputType'             => 'select',
            'options_callback'        => array('srhinow.beekeepingmanager.listener.dca.bkm_option_settings', 'getStatusOptions'),
			'eval'                  => array('tl_class'=>'clr w50', 'submitOnChange'=>false),
			'sql'					=> "varchar(32) NOT NULL default 'item'"
		),
		'frames' =>  array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['frames'],
			'exclude'                 => true,
			'inputType'               => 'multiColumnWizard',
			'eval' => array(
				// 'style'                 => 'width:100%;',
				'doNotCopy'=>true,
				'columnFields' => array
				(
					'sign' => array
					(
						'label'             => $GLOBALS['TL_LANG']['tl_bkm_hivemap']['sign'],
						'inputType'         => 'select',
						'options'   		=> &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['sign_options'],
						'eval'              => array( 'tl_class'=>'wizard','style' => 'width:50px;'),
					),
					'count' => array
					(
						'label'             => $GLOBALS['TL_LANG']['tl_bkm_hivemap']['count'],
						'inputType'         => 'text',
						'eval'              => array('style' => 'width:80px'),
						'default'			=> 0
					),
					'frame_types' => array
					(
						'label'             => $GLOBALS['TL_LANG']['tl_bkm_hivemap']['frame_types'],
						'inputType'         => 'select',
                        'options_callback'        => array('srhinow.beekeepingmanager.listener.dca.bkm_option_settings', 'getFrametypesOptions'),
						'eval'              => array('style' => 'width:350px;'),
					)
				)
			),
			'sql'				=> "blob NULL"
		),
 		'material' =>  array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['material'],
			'exclude'                 => true,
			'inputType'               => 'multiColumnWizard',
			'eval' => array(
				// 'style'                 => 'width:100%;',
				'doNotCopy'=>true,
				'columnFields' => array
				(
					'sign' => array
					(
						'label'             => $GLOBALS['TL_LANG']['tl_bkm_hivemap']['sign'],
						'inputType'         => 'select',
						'options'   		=> &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['sign_options'],
						'eval'              => array( 'tl_class'=>'wizard','style' => 'width:50px;'),
					),
					'count' => array
					(
						'label'             => $GLOBALS['TL_LANG']['tl_bkm_hivemap']['count'],
						'inputType'         => 'text',
						'eval'              => array('style' => 'width:80px'),
						'default'			=> 0
					),
					'material_types' => array
					(
						'label'             => $GLOBALS['TL_LANG']['tl_bkm_hivemap']['material_types'],
						'inputType'         => 'select',
                        'options_callback'  => array('srhinow.beekeepingmanager.listener.dca.bkm_option_settings', 'getMaterialOptions'),
						'eval'              => array('style' => 'width:350px;'),
					)
				)
			),
			'sql'				=> "blob NULL"
		),
        'medication' =>  array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['medication'],
			'exclude'                 => true,
			'inputType'               => 'multiColumnWizard',
			'eval' => array(
				'doNotCopy'=>true,
				'columnFields' => array
				(
					'sign' => array
					(
						'label'             => $GLOBALS['TL_LANG']['tl_bkm_hivemap']['sign'],
						'inputType'         => 'select',
						'options'   		=> &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['sign_options'],
						'eval'              => array( 'tl_class'=>'wizard','style' => 'width:50px'),
					),
					'count' => array
					(
						'label'             => $GLOBALS['TL_LANG']['tl_bkm_hivemap']['count'],
						'inputType'         => 'text',
						'eval'              => array('style' => 'width:80px'),
						'default'			=> 0
					),
					'volume' => array
					(
						'label'             => $GLOBALS['TL_LANG']['tl_bkm_hivemap']['volume'],
						'inputType'         => 'select',
                        'options_callback'        => array('srhinow.beekeepingmanager.listener.dca.bkm_option_settings', 'getVolumeOptions'),
						'eval'              => array( 'tl_class'=>'wizard','style' => 'width:100px'),
					),
					'medi_types' => array
					(
						'label'             => $GLOBALS['TL_LANG']['tl_bkm_hivemap']['medi_types'],
						'inputType'         => 'select',
                        'options_callback'        => array('srhinow.beekeepingmanager.listener.dca.bkm_option_settings', 'getMeditypeOptions'),
						'eval'              => array('style' => 'width:350px;'),
					)
				)
			),
			'sql'				=> "blob NULL"
		),
		'feed' =>  array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['feed'],
			'exclude'                 => true,
			'inputType'               => 'multiColumnWizard',
			'eval' => array(
				'doNotCopy'=>true,
				'columnFields' => array
				(
					'sign' => array
					(
						'label'             => $GLOBALS['TL_LANG']['tl_bkm_hivemap']['sign'],
						'inputType'         => 'select',
						'options'   		=> &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['sign_options'],
						'eval'              => array( 'tl_class'=>'wizard','style' => 'width:50px'),
					),
					'count' => array
					(
						'label'             => $GLOBALS['TL_LANG']['tl_bkm_hivemap']['count'],
						'inputType'         => 'text',
						'eval'              => array('style' => 'width:80px'),
						'default'			=> 0
					),
					'volume' => array
					(
						'label'             => $GLOBALS['TL_LANG']['tl_bkm_hivemap']['volume'],
						'inputType'         => 'select',
                        'options_callback'        => array('srhinow.beekeepingmanager.listener.dca.bkm_option_settings', 'getVolumeOptions'),
						'eval'              => array( 'tl_class'=>'wizard','style' => 'width:100px'),
					),
					'feed_types' => array
					(
						'label'             => $GLOBALS['TL_LANG']['tl_bkm_hivemap']['feed_types'],
						'inputType'         => 'select',
                        'options_callback'        => array('srhinow.beekeepingmanager.listener.dca.bkm_option_settings', 'getFeedtypeOptions'),
						'eval'              => array('style' => 'width:350px;'),
					)
				)
			),
			'sql'				=> "blob NULL"
		),
        'rating_breed' => array
        (
            'label'                 => &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['rating_breed'],
            'default'               => '',
            'exclude'               => true,
            'inputType'             => 'select',
            'options_callback'        => array('srhinow.beekeepingmanager.listener.dca.bkm_option_settings', 'getBreedQuantityOptions'),
            'eval'                  => array('tl_class'=>'w50', 'includeBlankOption'=>true),
            'sql'					=> "varchar(32) NOT NULL default ''"
        ),
        'rating_feed' => array
        (
            'label'                 => &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['rating_feed'],
            'default'               => '',
            'exclude'               => true,
            'inputType'             => 'select',
            'options_callback'        => array('srhinow.beekeepingmanager.listener.dca.bkm_option_settings', 'getFeedQuantityOptions'),
            'eval'                  => array('tl_class'=>'w50', 'includeBlankOption'=>true),
            'sql'					=> "varchar(32) NOT NULL default ''"
        ),
        'rating_gentleness' => array
        (
            'label'                 => &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['rating_gentleness'],
            'default'               => '',
            'exclude'               => true,
            'inputType'             => 'select',
            'options_callback'        => array('srhinow.beekeepingmanager.listener.dca.bkm_option_settings', 'getGentlenessOptions'),
            'eval'                  => array('tl_class'=>'w50', 'includeBlankOption'=>true),
            'sql'					=> "varchar(32) NOT NULL default ''"
        ),
        'rating_population' => array
        (
            'label'                 => &$GLOBALS['TL_LANG']['tl_bkm_hivemap']['rating_population'],
            'default'               => '',
            'exclude'               => true,
            'inputType'             => 'select',
            'options_callback'        => array('srhinow.beekeepingmanager.listener.dca.bkm_option_settings', 'getPopulationSizeOptions'),
            'eval'                  => array('tl_class'=>'w50', 'includeBlankOption'=>true),
            'sql'					=> "varchar(32) NOT NULL default ''"
        ),

	)
);
