<?php
/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['new'][0]                          = 'Neue Auswahl-Einstellung';
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['new'][1]                          = 'Eine neue BAuswahl-Einstellung anlegen.';
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['edit'][0]                         = 'Auswahl-Einstellung bearbeiten';
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['edit'][1]                         = 'Auswahl-Einstellung ID %s bearbeiten.';
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['copy'][0]                         = 'Auswahl-Einstellung duplizieren';
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['copy'][1]                         = 'Auswahl-Einstellung ID %s duplizieren.';
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['delete'][0]                       = 'Auswahl-Einstellung löschen';
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['delete'][1]                       = 'Auswahl-Einstellung ID %s löschen.';
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['show'][0]                         = 'Auswahl-Einstellungndetails anzeigen';
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['show'][1]                         = 'Details für Auswahl-Einstellung ID %s anzeigen.';


/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['name'] = ['Einstellungsname'];
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['fallback'] = ['Standart-Einstellungen'];
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['status_options'] = ['Status-Auswahl'];
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['frametype_options'] = ['Rähmchentyp-Auswahl'];
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['meditype_options'] = ['Medikamenten-Auswahl'];
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['volumes_options'] = ['Mengenangaben-Auswahl'];
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['bee_race_options'] = ['Bienenrassen-Auswahl'];
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['breed_quantity_options'] = ['Brutmengen-Auswahl'];
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['feed_quantity_options'] = ['Futtermengen-Auswahl'];
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['population_size_options'] = ['Bienenmasse-Auswahl'];
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['gentleness_options'] = ['Sanftmut-Auswahl'];
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['feed_options'] = ['Futtergabe-Auswahl'];
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['material_options'] = ['Material-Auswahl'];

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['options_legend'] = 'Auswahl-Einstellungen';

$GLOBALS['TL_LANG']['tl_bkm_option_settings']['default_status_options'] = array(
    1 => 'existiert',
    2 => 'aufgelöst',
    3 => 'tot'
);

$GLOBALS['TL_LANG']['tl_bkm_option_settings']['default_frametype_options'] = array
(
    'MW' => 'Mittelwand',
    'BR' => 'Baurahmen',
    'BW' => 'Brutwabe',
    'HW' => 'Honigwabe',
    'LW' => 'Leerwabe',
    'WBW' => 'Wildbauwabe'
);
$GLOBALS['TL_LANG']['tl_bkm_option_settings']['default_meditype_options'] = array
(
    'as' => 'Ameisensäure',
    'os' => 'Oxalsäure',
    'ms' => 'Milchsäure',

);

$GLOBALS['TL_LANG']['tl_bkm_option_settings']['default_volumes_options'] = array
(
    'ml' => 'ml',
    'l' => 'l',
    'g' => 'g',
    'kg' => 'kg',
    'stueck' => 'Stück'
);

$GLOBALS['TL_LANG']['tl_bkm_option_settings']['default_feed_options'] = array
(
    'zw' => 'Zuckerwasser',
    'ff' => 'Flüssigfutter',
    'ft' => 'Futterteig',
    'fw' => 'Futterwaben',
);

$GLOBALS['TL_LANG']['tl_bkm_option_settings']['default_bee_race_options'] = array
(
    '' => '-- unbekannt --',
    'Carnica' => 'Apis mellifera carnica',
    'Buckfast' => 'Buckfast',
    'Dunkle Europäische Biene' => 'mellifera mellifera Linnaeus',
    'Ligustica' => 'Apis mellifera ligustica',
);

$GLOBALS['TL_LANG']['tl_bkm_option_settings']['default_feed_quantity_options'] = array
(
    '1' => 'sehr gut',
    '2' => 'gut',
    '3' => 'befriedigend',
    '4' => 'schlecht',
    '5' => 'sehr schlecht',
);

$GLOBALS['TL_LANG']['tl_bkm_option_settings']['default_breed_quantity_options'] = array
(
    '1' => 'sehr gut',
    '2' => 'gut',
    '3' => 'befriedigend',
    '4' => 'schlecht',
    '5' => 'sehr schlecht',
);

$GLOBALS['TL_LANG']['tl_bkm_option_settings']['default_population_size_options'] = array
(
    '1' => 'eine Kelle voll',
    '2' => '2 besetzte Waben',
    '3' => '3 besetzte Waben',
    '4' => '4 besetzte Waben',
    '5' => '5 besetzte Waben',
    '6' => '6 besetzte Waben',
    '7' => 'knapp eine Zarge voll',
    '8' => 'etwa 1,5 Zargen besetzt',
    '9' => 'knapp 2 Zargen besetzt',
    '10' => 'quellen über',
);

$GLOBALS['TL_LANG']['tl_bkm_option_settings']['default_population_size_options'] = array
(
    '1' => 'eine Kelle voll',
    '2' => '2 besetzte Waben',
    '3' => '3 besetzte Waben',
    '4' => '4 besetzte Waben',
    '5' => '5 besetzte Waben',
    '6' => '6 besetzte Waben',
    '7' => 'knapp eine Zarge voll',
    '8' => 'etwa 1,5 Zargen besetzt',
    '9' => 'knapp 2 Zargen besetzt',
    '10' => 'quellen über',
);

$GLOBALS['TL_LANG']['tl_bkm_option_settings']['default_gentleness_options'] = array
(
    '1' => 'sehr friedlich',
    '2' => 'gut',
    '3' => 'einzelne nervös',
    '4' => 'schlechter',
    '5' => 'grenzwertig',
    '6' => 'agressiv'
);