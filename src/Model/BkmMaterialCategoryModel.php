<?php
/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace Srhinow\BeekeepingManagerBundle\Model;

use Contao\Model;

/**
 * Reads and writes Credit Items
 *
 * @property integer $id
 * @property integer $tstamp
 * @property string  $title
 *
 * @method static BkmMaterialCategoryModel|null findById($id, $opt=array())
 * @method static BkmMaterialCategoryModel|null findByIdOrAlias($val, $opt=array())
 * @method static BkmMaterialCategoryModel|null findByPk($id, $opt=array())
 * @method static BkmMaterialCategoryModel|null findOneBy($col, $val, $opt=array())
 * @method static BkmMaterialCategoryModel|null findOneByTstamp($val, $opt=array())
 * @method static BkmMaterialCategoryModel|null findOneByTitle($val, $opt=array())

 *
 * @method static \Model\Collection|BkmMaterialCategoryModel[]|BkmMaterialCategoryModel|null findByTstamp($val, $opt=array())
 * @method static \Model\Collection|BkmMaterialCategoryModel[]|BkmMaterialCategoryModel|null findByTitle($val, $opt=array())
 * @method static \Model\Collection|BkmMaterialCategoryModel[]|BkmMaterialCategoryModel|null findBy($col, $val, $opt=array())
 * @method static \Model\Collection|BkmMaterialCategoryModel[]|BkmMaterialCategoryModel|null findAll($opt=array())
 *
 * @method static integer countById($id, $opt=array())
 *
 * @author Leo Feyer <https://github.com/leofeyer>
 */


class BkmMaterialCategoryModel extends Model
{
	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_bkm_material_category';

}
