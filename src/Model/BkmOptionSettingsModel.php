<?php
/**
 * Created by c4.pringitzhonig.de.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 17.09.19
 */

namespace Srhinow\BeekeepingManagerBundle\Model;


use Contao\Model;

/**
 * Reads and writes Credit Items
 *
 * @property integer $id
 * @property integer $tstamp
 * @property string  $title
 *
 * @method static BkmMaterialModel|null findById($id, $opt=array())
 * @method static BkmMaterialModel|null findByIdOrAlias($val, $opt=array())
 * @method static BkmMaterialModel|null findByPk($id, $opt=array())
 * @method static BkmMaterialModel|null findOneBy($col, $val, $opt=array())
 * @method static BkmMaterialModel|null findOneByTstamp($val, $opt=array())
 * @method static BkmMaterialModel|null findOneByTitle($val, $opt=array())

 *
 * @method static \Model\Collection|BkmMaterialModel[]|BkmMaterialModel|null findByTstamp($val, $opt=array())
 * @method static \Model\Collection|BkmMaterialModel[]|BkmMaterialModel|null findByTitle($val, $opt=array())
 * @method static \Model\Collection|BkmMaterialModel[]|BkmMaterialModel|null findBy($col, $val, $opt=array())
 * @method static \Model\Collection|BkmMaterialModel[]|BkmMaterialModel|null findAll($opt=array())
 *
 * @method static integer countById($id, $opt=array())
 *
 * @author Leo Feyer <https://github.com/leofeyer>
 */

class BkmOptionSettingsModel extends Model
{
    /**
     * Table name
     * @var string
     */
    protected static $strTable = 'tl_bkm_option_settings';

}
