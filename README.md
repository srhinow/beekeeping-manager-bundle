# Mit diesem Contao-Bundle haben sie die Möglichkeit Ihre Bienenstöcke zu verwalten.

>Author: Sven Rhinow

>Web: https://gitlab.com/srhinow/beekeeping-manager-bundle

>License: LPGL-3.0

Es ist eine Weiterentwicklung des bis Contao 3.5 erstellten Erweiterung "beekeeping-managment"
